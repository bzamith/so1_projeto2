# so1_projeto2
Projeto Memory Managment no xv6 para a disciplina de Sistemas Operacionais

Segunda Prova de Sistemas Operacionais 1,
Professora Dra. Kelen Vivaldini
Primeiro Semestre 2018

## Alunos
* Bruna Zamith Santos (RA: 628093)
* Henrique Cordeiro Frajacomo (RA: 726536)
* João Victor Pacheco (RA: 594970)
* Marcos Augusto Faglioni Junior (RA: 628301)


## Execução 

Para executar o projeto é necessário ter instalado o emulador QEMU 
No ubuntu:
```shell
apt-get install qemu
```
Após a instalação execute o seguinte comando no diretório desejado do projeto para compilá-lo e executá-lo:
```shell
make qemu
```
Utilize o comando 
```shell
make clean
```
após a execução para excluir os arquivos gerados pela compilação.

Observe que existem diferentes diretórios na pasta do projeto, sendo estes:
- [x] /original = xv6 original, sem qualquer modificação, (para baixar: git clone https://github.com/mit-pdos/xv6-public.git)
- [x] /task1 = implementação da task1
- [x] /task2 = implementação da task2
- [x] /task2_teste = implementado apenas o procedimento nullpointer, para testarmos o task2 antes da solução
- [x] /task3 = implementação da task3
- [x] /task3_teste = implementado apenas o procedimento permissionexploit, para testarmos o task3 antes da solução
- [x] /task4 = implementação da task4
